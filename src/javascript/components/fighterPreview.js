import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  fighterElement.appendChild(createFighterImage(fighter));
  fighterElement.appendChild(createFighterInfo(fighter));

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

// create block for figher info (name, health, attack and defense)
function createFighterInfo(fighter) {
  const fighterInfoBlock = createElement({
    tagName: 'div',
    className: 'fighter-preview___info-block',
  });

  Object.keys(fighter).forEach(key => {
    if(key === 'name' || key === 'health' || key === 'attack' || key === 'defense') {
      fighterInfoBlock.appendChild(createInfoText(key, fighter[key]))
    }
  });

  return fighterInfoBlock;
}

// create text element for each key
function createInfoText(key, info) {
  let infoElement = createElement({
    tagName: 'p',
    className: 'fighter-preview___info-text'
  });
  infoElement.innerText = key[0].toUpperCase() + key.slice(1) + ': ' + info;

  return infoElement;
}