import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function 
  const { name } = fighter
  const title = `${name} WINS!`;

  const bodyElement = createElement({tagName: 'div', className: 'modal-body'});
  
  bodyElement.appendChild(createFighterImage(fighter));
  
  return showModal({title, bodyElement});
}
