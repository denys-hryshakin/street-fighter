import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const { PlayerOneAttack, PlayerTwoAttack, PlayerOneBlock, 
    PlayerTwoBlock, PlayerOneCriticalHitCombination, 
    PlayerTwoCriticalHitCombination } = controls;

  // copy first fighert object to get start health
  const firstFighterStartHealth = Object.assign({}, firstFighter);
  firstFighterStartHealth.health = firstFighter.health;

  // copy second fighert object to get start health
  const secondFighterStartHealth = Object.assign({}, secondFighter);
  secondFighterStartHealth.health = secondFighter.health;
  
  // check is block or not
  let isBlock = [false, false];

  //check for critical hit
  let isCriticalHit = [true, true];
  //create Sets for unique keys
  let firstFighterCriticalHit = new Set(), secondFighterCriticalHit = new Set();

  return new Promise((resolve) => {
    // key event listeners
    document.onkeyup = 
    document.onkeydown = (event) => {
      switch(event.type) {
        case 'keydown': {
          // 1st player attack
          if(!event.repeat && event.code === PlayerOneAttack) { 
            // 1st player can't attack in the block and 2nd player can't get damage in the block
            if(!isBlock[0] && !isBlock[1]) {
              secondFighter.health -= getDamage(firstFighter, secondFighter);
            }
          }
          // 2nd player attack
          if(!event.repeat && event.code === PlayerTwoAttack) {
            // 2nd player can't attack in the block and 1st player can't get damage in the block
            if(!isBlock[1] && !isBlock[0]) {
              firstFighter.health -= getDamage(secondFighter, firstFighter);
            }
          }
          
          // 1st player block while button is pressed
          if(event.code === PlayerOneBlock) {
            // change block status
            if(!isBlock[0]) isBlock[0] = true;
          }
          // 2nd player block while button is pressed
          if(event.code === PlayerTwoBlock) {
            if(!isBlock[1]) isBlock[1] = true;
          }

          // 1st player critical hit
          if(!event.repeat && PlayerOneCriticalHitCombination.includes(event.code) && !firstFighterCriticalHit.has(event.code)) {
            // check key for including in the combination and unique set; add a key if the condition is performed
            firstFighterCriticalHit.add(event.code);
          }
          // 2nd player critical hit
          if(!event.repeat && PlayerTwoCriticalHitCombination.includes(event.code) && !secondFighterCriticalHit.has(event.code)) {
            secondFighterCriticalHit.add(event.code);
          }

          // use method every for check if unique set include all need keys
          if(PlayerOneCriticalHitCombination.every(key => firstFighterCriticalHit.has(key)) && isCriticalHit[0]){
            // call getCriticalHit for 1st player if condition good 
            getCriticalHit(secondFighter, 'left', firstFighterCriticalHit, isCriticalHit, 0) 
          }
          if(PlayerTwoCriticalHitCombination.every(key => secondFighterCriticalHit.has(key)) && isCriticalHit[1]){
            getCriticalHit(firstFighter, 'right', secondFighterCriticalHit, isCriticalHit, 1)
          }

          break;
        };
        case 'keyup': {
          // 1st player out of block 
          if(event.code === PlayerOneBlock) {
            if(isBlock[0]) isBlock[0] = false;
          }
          // 2nd player out of block
          if(event.code === PlayerTwoBlock) {
            if(isBlock[1]) isBlock[1] = false;
          }
          
          break;
        };
      }

    // resolve the promise with the winner when fight is over
    if(firstFighter.health <= 0) {
      // stop key event listener after 1st player win
      document.onkeydown = document.onkeyup = null;
      resolve(secondFighter);
    } else if(secondFighter.health <= 0) {
      // stop key event listeners after 2nd player win
      document.onkeydown = document.onkeyup = null;
      resolve(firstFighter);
    } else {
      // change healthbar during the fight using startHealth variable
      document.getElementById('left-fighter-indicator').style.width = `${(firstFighter.health*100)/firstFighterStartHealth.health}%`;
      document.getElementById('right-fighter-indicator').style.width = `${(secondFighter.health*100)/secondFighterStartHealth.health}%`;
    };
  }
});
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower < blockPower ? 0 : hitPower - blockPower;
  
  return damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = Math.random() * (2-1) + 1; //(max-min)+min; max=2, min=1
  const hitPower = attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() * (2-1) + 1; //(max-min)+min; max=2, min=1
  const blockPower = defense * dodgeChance
  
  return blockPower;
}

// Critical Hit every 10 seconds
function getCriticalHit(fighter, position, criticalHit, isCriticalHit, i) {
  let criticalDamage = fighter.health -= getHitPower(fighter) * 2;
  // start timeout after key combination
  setTimeout(()=> {
    isCriticalHit[i] = true; // change critical hit status
    document.getElementById(`${position}-fighter-hit-indicator`).style.backgroundColor = '#f03434'; // change color of the bar
  }, 10000);
  document.getElementById(`${position}-fighter-hit-indicator`).style.backgroundColor = 'transparent'; // change color of the bar
  isCriticalHit[i] = false; // change critical hit status

  criticalHit.clear(); // clear Set()

  return criticalDamage;
}
